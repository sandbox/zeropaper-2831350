<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryVariableInstanceService extends BaseService {
  protected $name = 'History: variable instance';

  protected $path = 'history/variable-instance';
}
