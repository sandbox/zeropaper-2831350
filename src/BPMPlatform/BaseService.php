<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Middleware;

class BaseService implements ServiceInterface {
  protected $name;

  protected $path;

  protected $config;

  protected $client;

  public function __construct() {
    $this->config = \Drupal::service('config.factory')->getEditable('camunda_bpm_api.settings');
    $this->client = \Drupal::httpClient();
  }

  public function getName() {
    return $this->name;
  }

  public function getPath() {
    return $this->path;
  }


  protected function request($method, $payload, $path_suffix = '') {
    $platform_url = $this->config->get('platform_url');
    $url = $platform_url . '/' . $this->getPath() . $path_suffix;

    $options = [
      'json' => $payload,
      'headers' => [
        'Accept' => 'application/hal+json, application/json; q=0.5'
      ]
    ];

    $needAuth = (boolean) $this->config->get('platform_need_auth');
    if ($needAuth) {
      $options['auth'] = [
        $this->config->get('platform_username'),
        $this->config->get('platform_password')
      ];
    }

    try {
      $data = (string) $this->client
        ->request(strtoupper($method), $url, $options)
        ->getBody();

      $data = (array) json_decode($data, TRUE);

      return $data;
    }
    catch(RequestException $exception) {
      \Drupal::logger('camunda_bpm_api')->error('Request error: !errmsg.', array('!errmsg' => $exception->getMessage()));
      if ($this->config->get('print_errors')) {
        drupal_set_message(t('An error occured while performing request: @errmsg.', array('@errmsg' => $exception->getMessage())), 'error');
      }
    }
  }












  public function options($id = NULL, $payload = NULL) {
    drupal_set_message(t('Method %method is not implemented on %serviceName', array(
      '%method' => 'options',
      '%serviceName' => $this->name
    )), 'error');
  }


  public function get($id) {
    $idPath = '';
    if (!empty($id)) {
      $idPath = '/' . $id;
    }

    return $this->request('get', array(), $idPath);
  }

  public function post($payload, $path_suffix = '') {
    return $this->request('post', $payload, $path_suffix);
  }

  public function getList($payload = array(), $post = FALSE) {
    $method = 'get';
    if ($post) {
      $method = 'post';
    }
    return $this->request($method, $payload);
  }

  public function getListCount($payload = array(), $post = FALSE) {
    $method = 'get';
    if ($post) {
      $method = 'post';
    }
    return $this->request($method, $payload, '/count');
  }



  public function put($id = NULL, $payload = NULL) {
    drupal_set_message(t('Method %method is not implemented on %serviceName', array(
      '%method' => 'put',
      '%serviceName' => $this->name
    )), 'error');
  }



  public function delete() {
    drupal_set_message(t('Method %method is not implemented on %serviceName', array(
      '%method' => 'delete',
      '%serviceName' => $this->name
    )), 'error');
  }
}