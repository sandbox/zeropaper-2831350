<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class TaskVariablesService extends BaseService {
  protected $name = 'Task variables';

  protected $path = 'task';


  public function getBinary($procInstId, $varName) {
    return $this->request('get', array(), '/' . $procInstId . '/variables/' . $varName . '/data');
  }


  public function postBinary($procInstId, $varName, $payload = array()) {
    return $this->request('post', $payload, '/' . $procInstId . '/variables/' . $varName . '/data');
  }


  public function modify($procInstId, $varName, $payload = array()) {
    return $this->request('post', $payload, '/' . $procInstId . '/variables/' . $varName);
  }


  public function update($procInstId, $varName, $payload = array()) {
    return $this->request('put', $payload, '/' . $procInstId . '/variables/' . $varName);
  }
}
