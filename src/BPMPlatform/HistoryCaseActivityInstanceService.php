<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryCaseActivityInstanceService extends BaseService {
  protected $name = 'History: case activity instance';

  protected $path = 'history/case-activity-instance';
}
