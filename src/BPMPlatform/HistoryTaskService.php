<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryTaskService extends BaseService {
  protected $name = 'History: task';

  protected $path = 'history/task';
}
