<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryBatchService extends BaseService {
  protected $name = 'History: batch';

  protected $path = 'history/batch';
}
