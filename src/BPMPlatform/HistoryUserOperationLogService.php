<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryUserOperationLogService extends BaseService {
  protected $name = 'History: user operation log';

  protected $path = 'history/user-operation';
}
