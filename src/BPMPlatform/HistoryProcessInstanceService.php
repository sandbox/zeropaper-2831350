<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryProcessInstanceService extends BaseService {
  protected $name = 'History: process instance';

  protected $path = 'history/process-instance';
}
