<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class ProcessInstanceService extends BaseService {
  protected $name = 'Process Instance';

  protected $path = 'process-instance';


  public function getActivityInstance($procInstId) {
    return $this->request('get', array(), '/' . $procInstId . '/activity-instance');
  }
}
