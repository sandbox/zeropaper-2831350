<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryJobLogService extends BaseService {
  protected $name = 'History: job log';

  protected $path = 'history/job-log';
}
