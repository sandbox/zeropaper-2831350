<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryIncidentService extends BaseService {
  protected $name = 'History: incident';

  protected $path = 'history/incident';
}
