<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class ProcessInstanceVariablesService extends SubService {
  protected $name = 'Process instance variables';

  protected $path = 'variables';

  protected $parentPath = 'process-instance';

  public function getBinary($varName) {
    return $this->request('get', array(), $varName . '/data');
  }


  public function postBinary($varName, $payload = array()) {
    return $this->request('post', $payload, $varName . '/data');
  }


  public function modify($varName, $payload = array()) {
    return $this->request('post', $payload, $varName);
  }


  public function update($varName, $payload = array()) {
    return $this->request('put', $payload, $varName);
  }
}
