<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryCaseDefinitionService extends BaseService {
  protected $name = 'History: case definition';

  protected $path = 'history/case-definition';
}
