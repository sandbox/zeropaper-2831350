<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class ProcessDefinitionService extends BaseService {
// class ProcessDefinitionService implements ServiceInterface {
  protected $name = 'Process Definition';

  protected $path = 'process-definition';



  public function getActivityInstanceStatistics($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/statistics');
  }

  public function getActivityInstanceStatisticsByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/statistics');
  }

  public function getActivityInstanceStatisticsByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/statistics');
  }



  public function getDiagram($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/diagram');
  }

  public function getDiagramByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/diagram');
  }

  public function getDiagramByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/diagram');
  }



  public function getFormVariables($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/form-variables');
  }

  public function getFormVariablesByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/form-variables');
  }

  public function getFormVariablesByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/form-variables');
  }



  public function getRenderedStartForm($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/rendered-form');
  }

  public function getRenderedStartFormByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/rendered-form');
  }

  public function getRenderedStartFormByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/rendered-form');
  }



  public function getStartFormKey($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/startForm');
  }

  public function getStartFormKeyByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/startForm');
  }

  public function getStartFormKeyByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/startForm');
  }


  public function getProcessInstanceStatistics() {
    return $this->request('get', array(), '/statistics');
  }


  public function getXML($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/xml');
  }

  public function getXMLByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/xml');
  }

  public function getXMLByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/xml');
  }


  public function getStartInstance($procDefId) {
    return $this->request('get', array(), '/' . $procDefId . '/start');
  }

  public function getStartInstanceByKey($procDefKey) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/start');
  }

  public function getStartInstanceByKeyTenant($procDefKey, $tenantId) {
    return $this->request('get', array(), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/start');
  }



  public function submitStartForm($id, array $variables, $businessKey = NULL) {
    return $this->post(array(
      'businessKey' => $businessKey,
      'variables' => $variables
    ), '/' . $id . '/submit-form');
  }

  public function submitStartFormByKey($procDefKey, array $variables, $businessKey = NULL) {
    return $this->post(array(
      'businessKey' => $businessKey,
      'variables' => $variables
    ), '/key/' . $procDefKey . '/submit-form');
  }

  public function submitStartFormByKeyTenant($procDefKey, $tenantId, array $variables, $businessKey = NULL) {
    return $this->post(array(
      'businessKey' => $businessKey,
      'variables' => $variables
    ), '/key/' . $procDefKey . '/tenant-id/' . $tenantId . '/submit-form');
  }

}
