<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryIdentityLinkLogService extends BaseService {
  protected $name = 'History: identity link log';

  protected $path = 'history/identity-link-log';
}
