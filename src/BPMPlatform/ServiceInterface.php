<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

interface ServiceInterface {
  public function getName();

  public function getPath();

  public function options($id = NULL, $payload = NULL);

  public function post($payload, $path_suffix = '');

  public function get($id);

  public function getList($payload = array(), $post = FALSE);

  public function getListCount($payload = array(), $post = FALSE);

  public function put($id = NULL, $payload = NULL);
}