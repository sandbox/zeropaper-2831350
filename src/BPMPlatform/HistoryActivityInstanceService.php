<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryActivityInstanceService extends BaseService {
  protected $name = 'History: activity instance';

  protected $path = 'history/activity-instance';
}
