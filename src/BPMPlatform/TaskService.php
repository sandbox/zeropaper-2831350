<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class TaskService extends BaseService {
  protected $name = 'Task';

  protected $path = 'task';
}
