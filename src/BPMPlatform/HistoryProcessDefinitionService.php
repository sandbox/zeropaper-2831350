<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryProcessDefinitionService extends BaseService {
  protected $name = 'History: process definition';

  protected $path = 'history/process-definition';
}
