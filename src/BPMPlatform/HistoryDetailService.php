<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryDetailService extends BaseService {
  protected $name = 'History: detail';

  protected $path = 'history/detail';
}
