<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryDecisionRequirementsDefinitionService extends BaseService {
  protected $name = 'History: decision requirement definition';

  protected $path = 'history/decision-requirements-definition';

  public function get($id) {
    return $this->request('get', array(), '/' . $id . '/statistics');
  }
}
