<?php

namespace Drupal\camunda_bpm_api\BPMPlatform;

class HistoryCaseInstanceService extends BaseService {
  protected $name = 'History: case instance';

  protected $path = 'history/case-instance';
}
