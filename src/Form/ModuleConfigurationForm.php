<?php

namespace Drupal\camunda_bpm_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'camunda_bpm_api_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'camunda_bpm_api.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('camunda_bpm_api.settings');

    $default_platform_url = 'http://localhost:8080/camunda/api/engine/engine/default';
    $platform_url = $config->get('platform_url');
    if (empty($platform_url)) {
      $platform_url = $default_platform_url;
    }

    $form['platform_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Platform REST base URL'),
      '#default_value' => $platform_url,
      '#placeholder' => $default_platform_url,
      '#description' => $this->t('Probably something like:<ul><li>:distroUrl (for distributions)</li><li>:devUrl (for development setups)</li></ul>', array(
        ':distroUrl' => 'http://localhost:8080/engine-rest',
        ':devUrl' => 'http://localhost:8080/camunda/api/engine/engine/default'
      )),
      '#required' => TRUE
    );

    // very bad idea indeed
    $needAuth = (boolean) $config->get('platform_need_auth');
    $form['platform_need_auth'] = array(
      '#type' => 'checkbox',
      '#default_value' => $needAuth,
      '#title' => $this->t('Need authentication?')
    );
    $form['platform_auth'] = array(
      '#states' => array(
        'visible' => array(
          ':input[name="platform_need_auth"]' => array('checked' => TRUE),
        ),
      ),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => !$needAuth,
      '#title' => $this->t('Authentication')
    );
    $form['platform_auth']['platform_username'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Platform user name'),
      '#default_value' => $config->get('platform_username'),
      '#placeholder' => 'jonny1'
    );
    $form['platform_auth']['platform_password'] = array(
      '#type' => 'password',
      '#title' => $this->t('Platform user password'),
      '#default_value' => $config->get('platform_pass'),
      '#placeholder' => 'Please'
    );
    /*
    $form['platform_auth']['test'] = array(
      '#type' => 'button',
      '#value' => $this->t('Test credentials')
    );
    */

    $form['print_errors'] = array(
      '#type' => 'checkbox',
      '#title' => t('Print errors'),
      '#description' => t('Show a message to the user when a request fails.'),
      '#default_value' => $config->get('print_errors')
    );

    return parent::buildForm($form, $form_state);
  }



  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    if ($values['platform_need_auth'] && empty($values['platform_password'])) {
      $form_state->setErrorByName('platform_password', $this->t('Missing password'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $config = $this->config('camunda_bpm_api.settings');
    $config
      ->set('print_errors', $values['print_errors'])
      ->set('platform_url', $values['platform_url'])
      ->set('platform_need_auth', $values['platform_need_auth']);

    if (!empty($values['platform_need_auth'])) {
      $config
        ->set('platform_username', $values['platform_username'])
        ->set('platform_password', $values['platform_password']);
    }
    else {
      $config
        ->set('platform_password', NULL);
    }

    $config
      ->save();
  }
}